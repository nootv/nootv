#!/usr/bin/env python
# _*_ coding: utf-8 _*_

#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
#   Copyright (C) 2004 Sam Hocevar
#  14 rue de Plaisance, 75014 Paris, France 
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO. 

#Vootv is a burgundy TV channel
#This channel has a fucking website with a lot of Flash(tm) applet
#This script displays schedules and programs and permit you to download it
#You need: python, python-elixir, flvdump/rtmpdump and a good media player
#Enjoy

import time, sys
import HTMLParser, urllib
import os
from elixir import (Entity, Field, UnicodeText, Boolean, ManyToOne, OneToMany, 
        metadata, setup_all, create_all, session)

#test if flvdump or rtmpdump are installed
flvdumps = ['flvstreamer_x86', 'flvstreamer', 'rtmpdump_x86', 'rtmpdump']
def which(program):
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

find_flv=False
for tflvdump in flvdumps:
    flvdump = which(tflvdump)
    if flvdump != None:
        find_flv=True
        break

if find_flv == False:
    raise Exception('flvstreamer or rmpdump not found, please install it in \
            PATH')

def str_to_utf8(name):
    """
    Convert str to utf8
    """
    return unicode(name, 'utf-8') 

class Schedule(Entity):
    """
    Elixir database for Schedule
    """
    name = Field(UnicodeText)
    programs = OneToMany('Program')
    def is_program(self, name):
        name = str_to_utf8(name)
        if Program.query.filter_by(schedule=self, name=name).first():
            return True
        return False

    def add_program(self, name, link):
        name = str_to_utf8(name)
        link = str_to_utf8(link)
        return Program(name=name, link=link, schedule=schedule, isread=False)

    def get_programs(self):
        return self.programs

class Program(Entity):
    """
    Elixir database for Program
    """
    name = Field(UnicodeText)
    link = Field(UnicodeText)
    isread = Field(Boolean)
    schedule = ManyToOne('Schedule')
    def read(self):
        self.isread = True

metadata.bind = "sqlite:///nootv.sqlite"
setup_all()
create_all()

def add_schedule(name):
    """
    Add new schedule
    """
    name = str_to_utf8(name)
    sched = Schedule.query.filter_by(name=name).first()
    if sched == None:
        return Schedule(name=name)
    return sched

def get_schedule(name=None):
    """
    Get a schedule or all schedules
    """
    if name == None:
        return Schedule.query.all()
    return Schedule.query.filter_by(name=name).first()

def get_schedule_by_id(id):
    """
    Get schedule with elixir id
    """
    try:
        id=int(id)
    except:
        print('Numéro invalide')
        return None
    return Schedule.query.filter_by(id=id).first()

def get_program_by_id(id):
    """
    Get program with elixir id
    """
    try:
        id=int(id)
    except:
        print('Numéro invalide')
        return None
    return Program.query.filter_by(id=id).first()

class linkParser(HTMLParser.HTMLParser):
    """
    Silly class to retrieve fucking link
    God luck if you want read this...
    """
    def __init__(self, startwith="", typ="a", bal="href", first=None):
        HTMLParser.HTMLParser.__init__(self)
        self.islink=False
        self.links = {}
        self.firstlink = None
        self.startwith = startwith
        self.typ = typ
        self.bal = bal
        self.first = first
        self.isTitle = False
        self.title = None
    def handle_starttag(self, tag, attrs):
        if self.first != None and self.firstlink == None:
            if tag=='a':
                href = dict(attrs)['href']
                if href[:len(self.first)] == self.first:
                    self.firstlink = href
        if self.typ == 'img':
            if tag == 'h1':
                self.isTitle=True
        if tag==self.typ:
            href = dict(attrs)[self.bal]
            if href[:len(self.startwith)] == self.startwith:
                self.link = href
                self.islink=True
    def handle_data(self, data):
        if self.isTitle == True:
            self.title=data.strip()
            self.isTitle = False
        if self.islink == True:
            data = data.strip()
            if data == '':
                if self.title == None:
                    name=self.link
                else:
                    name=self.title
                    self.title=None
            else:
                name=data
            self.links[name] = self.link
            self.islink=False
 
new_prog = []

print('Téléchargement ...')
#search all Schedule in day pages
days=['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
#debug (no search)
#days=[]
schedules = []
links = []
for day in days:
    htmlSource = urllib.urlopen("http://www.vootv.fr/grille.php?jour=%s"%day\
            ).read(200000)
    p = linkParser('video.php?key=')
    p.feed(htmlSource)
    for name in p.links:
        if not name in schedules:
            schedules.append(name)
            links.append((name, p.links[name]))

#search all programs in a schedule
for name, link in links:
    schedule = add_schedule(name)
    #in first page of schedule
    htmlSource2 = urllib.urlopen("http://www.vootv.fr/%s" % link).read(200000)
    m = linkParser('/videoimages/', 'img', 'src', '/video.php?key=')
    m.feed(htmlSource2)
    first=None
    for link in m.links:
        name = m.links[link][13:][:-4]
        name = name.replace("\\\'", "'")
        if not schedule.is_program(link):
            prog = schedule.add_program(link, name)
            new_prog.append(prog)

    #in second page of schedule (for current program)
    htmlSource3 = urllib.urlopen("http://www.vootv.fr/%s" % m.firstlink\
            ).read(200000)
    m = linkParser('/videoimages/', 'img', 'src')
    m.feed(htmlSource3)
    for link in m.links:
        name = m.links[link][13:][:-4]
        name = name.replace("\\\'", "'")
        if not schedule.is_program(link):
            prog = schedule.add_program(link, name)
            new_prog.append(prog)
#save db
session.commit()
session.flush()

def display_menus(title, items, empty_message='vide pour quitter'):
    """
    display menu with title, list of items and prompt
    """
    print ('')
    print (title)
    for item in items:
        disp = '  %s' % str(item.id)
        if hasattr(item, 'isread'):
            if item.isread == True:
                disp += '*'
            else:
                disp += ' '
        disp += ' : %s' % item.name
        if hasattr(item, 'schedule'):
            print ('%s (%s)' % (disp, item.schedule.name))
        else:
            print (disp)
    print(empty_message)
    prompt = ">>> "
    rep = raw_input(prompt)
    return rep

#display menus until break
while 1:
    #if new programs, display it
    if new_prog != []:
        rep = display_menus("Nouveaux programmes", new_prog, 'vide pour voir les anciennes émissions')
        if rep == '':
            new_prog = []
    else:
        #display all Schedules
        rep = display_menus("Emissions", get_schedule())
        if rep == '':
            break
        rep = get_schedule_by_id(rep)
        if rep == None:
            print 'Numéro inconnu'
        else:
            #display all programs
            rep = display_menus("Programmes", rep.programs, 'vide pour retour aux émissions')
    if rep != '' and rep != None:
        #if program selected
        rep = get_program_by_id(rep)
        if rep == None:
            print 'Numéro inconnu'
        else:
            rep.read()
            #save db
            session.commit()
            session.flush()
            #if program is valid
            rep = rep.link

            url = "rtmpt://91.121.132.131/vod/vootv/vootv/%s" % rep
            print('#####################################################################')
            print('Enregistrement de "%s" dans le fichier nootv.flv' % url)
            print
            os.system('%s -q -r "%s" -c 80 > nootv.flv' % (flvdump, url))
            print('Vidéo enregistrée.')
            print('#####################################################################')

# vim: ts=4 sw=4 expandtab
